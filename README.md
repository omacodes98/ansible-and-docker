# Ansible & Docker 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create AWS EC2 instance with Terraform 

* Write Anbible Playbook that installs necessary technologies like Docker and Docker Compose, copies docker-compose file to the server and starts the Docker containers configured inside docker-compose file 

## Technologies Uesed 

* Ansible 

* AWS 

* Docker 

* Terraform 

* Linux 

## Steps 

Step 1: Use terraform to automate AWS infastructure to create cloud server 

[Terraform config file](/images/01_use_terraform_to_automate_terraforms_infastructure_to_create_cloud_server.png)

[Server on AWS console](/images/02_server_on_aws_console.png)

Step 2: Add cloud server ip address, private key location and user into hosts file 

[Host file](/images/03_add_cloud_server_ip_address_private_key_location_and_user_into_hosts_file.png)

Step 3: Create a playbook 

[Created playbook file](/images/04_create_a_playbook.png)

Step 4: Name first play and give play the host name or cloud server ip address 

[1st play](/images/05_name_first_play_and_give_play_the_host_name_or_cloud_server_ip_address.png)

Step 5: Set become attribute to yes, when doing this you dont have to set become_user to root because it is set to root user on default

[1st play become](/images/06_set_become_attribute_to_yes_when_doing_this_you_dont_have_to_set_become_user_to_root_because_it_is_set_to_root_user_on_default.png)

Step 6: Name first task which will be used for installing docker module with yum 

[1st play: task one name](/images/07_name_first_task_which_will_be_used_for_installing_docker_with_module_yum.png)

Step 7: In the first task call yum module to install docker

[1st play: task module](/images/07_name_first_task_which_will_be_used_for_installing_docker_with_module_yum.png)

Step 8: Test playbook, i didnt have to pass host file because i have configured that in ansible.cfg as a default inventory path 

    ansible-playbook deploy-docker.yaml

[Test play-book](/images/08_test_play_book_we_dont_have_to_pass_the_hosts_file_because_we_have_configured_that_in_ansible_config_as_a_default_inventory_path.png)

notes: This play should cause ansible to get a warning that python version 2 is the version installed on server,this isnt good meaning you would have to install python 3 on the server 

[Python 2 warning](/images/09_playbook_result_which_warns_us_that_python_version_2_is_the_version_installed_on_server_this_isnt_good_which_means_we_will_have_to_install_python3_on_the_server.png)

Step 9: Because of the error in last step make the first play install python 3 so name play and insert host 

[New 1st play name and host](/images/10_because_of_this_lets_make_the_first_play_install%20python3_so_name_play_and_insert_host.png)

Step 10: Set become attribute to yes 

[New 1st play become attribute](/images/11_become_root_setting_become_attribute_to_yes.png)

Step 11: Add task name and use module yum to install python 3 on cloud server

[New 1st play: task one](/images/12_Add_task_name_and_use_yum_module_to_install_python3_on_cloud_server.png)

Step 12: Go to ansible.cfg and set the interpreter to python3 

[ansible.cfg: interpreter=python3](/images/13_after_that_go_to_your_ansible_cfg_and_set_the_interpreter_to_python3.png)

Step 13: however when running this on a frsh server that doesnt have python3 yet installed ansible will give errors becuase it wouldnt beable to find python3 on cloud server, this happens because it hasnt been installed so you can change intepreter on a task level for the first plays task which is installing python3 

[Chaning intepreter on task level](/images/14_however_when_running_this_on_a_fresh_server_that_doesnt_have_python3_yet_installed_ansible_will_give_errors_because_it_wouldnt_be_able_to_find_python3_on_cloud_server_because_it_hasnt_been_installed_so_what_we_can_do_is_change_intepreter_on_a_task_.png)

Step 14: Create 3rd play name play and insert host

[3rd play: name and host](/images/16_create_3rd_play_name_play_and_insert_host.png)

Step 15: Set become attribute to yes so ansible executes tasks as root user 

[3rd play: become attribute](/images/17_set_become_attribute_to_yes_so_ansible_executes_tasks_as_root.png)

Step 16: Create  first task for 3rd play which will use get_url module to download docker-compose 

[3rd play: first task](/images/18_create_first_task_for_3rd_play_whichwill_use_get_url_module_to_download_docker-compose.png)

Step 17: Create 4th play name and insert host for starting docker

[4th play: name and host](/images/19_create_4th_play_to_start_docker_name_play_and_insert_host.png)

Step 18: Set become attribute to yes to execute tasks as root user fo 4th play

[4th play: become attribute](/images/20_set_become_attribute_to_yes_to_execute_commands_as_root.png)

Step 19: Add first task for 4th play by using systemd module to start docker 

[4th play: first task](/images/21_add_first_task_for_4th_play_by_using_systemd_module_to_start_docker.png)

Step 20: Create 5th play for adding ec2-user to docker group, give play name, insert host and set become attribute to yes to run tsks as root user 

[5th play: name host and become attribute](/images/22_create_5th_play_for%20adding%20ec2-user_to_docker_group_give_play_name_and_insert_host_also_set_become_attribute_to_yes_to_run_tasks_as_root_user.png)

Step 21: Add first task for fifth play using user module to add ec2-user to docker group 

[5th play: first task](/images/23_add_1st_task_for_fifth_play_using_uder_module_to_add_ec2-user_to_docker_group.png)

Step 22: Add second task for fifth play, this wil use meta module to reconnect to the server this is becuase when you add user to group the server does not recognise changes up until you reconnect to the server 

[5th play: second task](/images/24_add_second_task_that_will_use_meta_module_to_reconnect_to_the_server_we_need_to_do_this_because_when_we_add_user_to_group_the_server_doesnt_recognise_changes_up_until_you_reconnect_to_the_server.png)

Step 23: Create 6th play which will be specifically for testing if docker is working right, name play  and insert host

[6th play: name and host](/images/25_create_a_6th_play_which_will_be_specifically_for_testing_if_docker_is_working_right_name_play_and_insert_host.png)

Step 24: Name 6th play first task which will call module docker_image to pull redis image, this is just to test if docker is working

[6th play: first task](/images/26_name_first_task_and_call_module_docker_image_to_pull_redis_image_this_is_just_to_test_if_docker_is_working.png)

note: The problem with this is that when running the play in the docker test play it gives error for first task because we need to use pip to indtall a python module on the host server 

[6th play: first task error](/images/27_the_problem_with_this_is_that_when_running_the_play_in_the_docker_test_play_it_gives_error_for_first_task_because_we_need_to_use_pip_to_install_a_python_module_on_the_host_server.png)

Step 25: Because of this error we have Create 7th play for installing docker python module name play, insert host and set becom attribute to yes 

[7th play: name host and become attribute](/images/28_because_of_this_we_have_to_create_7th_play_name_play_and_insert_host_also_set_become_to_yes_to_be_able_to_install_as_root.png)

Step 26: Create first task for 7th play, name task and use module pip to install docker

[7th play: first task](/images/29_create_first_task_for_7th_play_name_task_and_use_module_pip_to_install_docker.png)

[Testing playbook 7th play mark](/images/30_run_playbook_to_ckeck_if_6th_(testing_docker_pull)_and_7th_(installing_python_module_docker)_play_works.png)

Step 27: Create 8th play to start docker container, name play and insert host

[8th play: name and host](/images/31_create_8th_play_to_start_docker_container_name_play_ans_insert_host.png)

Step 28: Name first task and use module copy to copy docker compose file from local computer to cloud server

[8th play: first task](/images/32_name_first_task_and_use_module_copy_to_copy_docker_compose_file_from_local_computer_to_cloud_server.png)

Step 29: Create second task for 8th play to log into docker, this is because image is in private docker repo, set name for second task and use module docker login to log into docker 

[8th play: second task](/images/33_create_2nd_task_to_log_into_docker_this_is_because_image_is_in_private_docker_repo_set_name_for_2nd_task_and_use_module_docker_login_to_log_into_docker.png)

Step 30: Set docker password in vars file 

[Docker password in vars file](/images/34_set_docker_password_in_vars_file.png)

Step 31: Use attribute vars_file and reference your vars file with the password 

[Referencing docker password in vars file](/images/35_use_attribute_vars_file_and_reference_your_vars_file_with_the_password.png)

Step 32: Add docker-compose as part of what pip is going to install in the play for installing python modules

[pip install docker-compose](/images/36_add_docker-compose_as_part_of_what_pip_is_going_to_install_in_the_play_for_installing_python_modules.png)

Step 33: Add third task for 8th play to run docker-compose and use module docker_compose to start container

[8th play: third task](/images/37_add_third_task_to_run_docker_compose_name_task_and_use_module_docker_compose_to_start_containers.png)

[play-book test 8th play mark](/images/38_run_play_book_to_test_new_play_and_tasks.png)

Step 34: Set attribute gather facts to false for first play, this is because on a fresh new server ansible will give an error at this part even if we set it to use python2 for first play so because of this set it to false to skip the first gathering facts

[1st play: gathering_facts attribute](/images/39_set_attribute_gather_facts_to_false_for_first_play_this_is_because_on_a_fresh_new_server_ansible_will_give_an_error_at_this_part_even_if_we_set_it_to_use_python2_for_first_play_so_because_of_this_set_it_to_false_to_skip_the_first_gathering_facts_ta.png)

Step 35: Make file more generic so you can use it for other distribution, do this by removing the ec2 user play to create user and have a task that uses user module to create user and insert it into docker and admin group

[Making file more generic](/images/40_make_file_more_generic_so_we_can_use_it_for_other_distribution_do_this_by_removing_the_ec2_user_play_to_create_user_and_have_a_task_that_uses_user_module_to_create_user_and_insert_it_into_docker_and_admin_group.png)

Step 36: In the start of docker container play set beome attribute to yes and become_user attribute to any name you want

note: change any part of the play that has ec2-user to new username 

[Making file more generic 2](/images/41_in_the_start_docker_container_play_set_become_attribute_to_yes_and_become_user_attribute_to_oma_change_any_part_of_the_play_that_has_ec2-user_to_new_username.png)

## Installation

    brew install ansible 

## Usage 

    ansible-playbook -i host deploy-node.yaml 

## How to Contribute

1. Clone the repo using $ git clone https://gitlab.com/omacodes98/ansible-and-docker.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-and-docker

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.